package servlet.view;

import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author BirkeHeeren
 */
@WebServlet(name = "ImageServlet", urlPatterns = {"/images/*"})
public class ImageServlet extends HttpServlet {

    private static final Map<String, byte[]> blobs = new HashMap<>();

    @Override
    public void init(ServletConfig config) throws ServletException {

        super.init(config);

        Iterator<String> iterator = config.getServletContext().getAttributeNames().asIterator();
        while (iterator.hasNext()) {
            String attribute = iterator.next();
            if (attribute.startsWith("pizza") && attribute.endsWith(".jpg")) {

                byte[] blob = (byte[]) config.getServletContext().getAttribute(attribute);
                blobs.put(attribute, blob);
            }
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try (OutputStream os = response.getOutputStream()) {
            response.setContentType("image/jpeg");
            
            String[] parts = request.getRequestURL().toString().split("/");
            os.write(blobs.get(parts[parts.length - 1]));
            os.flush();
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        // Weiterleiten des Requests an die Methode doGet()
        doGet(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
