package servlet.controller;

import art.dornrose.helper.ControllerHelper;
import art.dornrose.enums.StatusLevel;
import art.dornrose.logging.LoggerHelper;
import art.dornrose.model.Bestellung;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

/**
 *
 * @author BirkeHeeren
 */
@WebServlet(name = "BestellungInsert", urlPatterns = {"/BestellungInsert"})
public class BestellungInsert extends HttpServlet {

    private DataSource dataSource;
    private String user;
    private String pwd;

    @Override
    public void init(ServletConfig config) throws ServletException {

        super.init(config);

        ServletContext ctx = config.getServletContext();
        user = ctx.getInitParameter("dbUser");
        pwd = ctx.getInitParameter("dbPassword");
        dataSource = (DataSource) ctx.getAttribute("dataSource");
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        if (request.getSession().getAttribute(ControllerHelper.LOGGEDIN) == null 
                || !(Boolean) request.getSession().getAttribute(ControllerHelper.LOGGEDIN)) {
            response.sendRedirect("index.jsp");
            return;
        }

        ((Bestellung) request.getSession().getAttribute(ControllerHelper.BESTELLUNG)).setUuid(UUID.randomUUID());
        
        final UUID UUID_BESTELLUNG = ((Bestellung) request.getSession().getAttribute(ControllerHelper.BESTELLUNG)).getUuid();
        final String ID_KUNDE = ((Bestellung) request.getSession().getAttribute(ControllerHelper.BESTELLUNG)).getKunde().getSessionid();
        final String IP_ADRESSE = ((Bestellung) request.getSession().getAttribute(ControllerHelper.BESTELLUNG)).getIpadresse();

        String SQL_INSERT_BESTELLUNG = "INSERT INTO bestellungen (uuid, kunde, ipadresse, datum) VALUES ( ?,?,?, NULL);";

        try (Connection connection = dataSource.getConnection(user, pwd)) {

            PreparedStatement preparedStatementInsertBestellung = connection.prepareStatement(SQL_INSERT_BESTELLUNG);
            preparedStatementInsertBestellung.setString(1, UUID_BESTELLUNG.toString());
            preparedStatementInsertBestellung.setString(2, ID_KUNDE);
            preparedStatementInsertBestellung.setString(3, IP_ADRESSE);
            preparedStatementInsertBestellung.executeQuery();

            for (int pizzaid : ((Bestellung) request.getSession().getAttribute(ControllerHelper.BESTELLUNG)).getBestellungMap().keySet()) {
                if (((Bestellung) request.getSession().getAttribute(ControllerHelper.BESTELLUNG)).getBestellungMap().get(pizzaid).getAmount() > 0) {

                    String amount = String.valueOf(((Bestellung) request.getSession().getAttribute(ControllerHelper.BESTELLUNG)).getBestellungMap().get(pizzaid).getAmount());
                    
                    String SQL_INSERT_ITEMS = "INSERT INTO bestellung_pizza (bestellung, pizza, amount) VALUES (?, ?, ?);";
                    PreparedStatement preparedStatementInsertItems = connection.prepareStatement(SQL_INSERT_ITEMS);
                    preparedStatementInsertItems.setString(1, UUID_BESTELLUNG.toString());
                    preparedStatementInsertItems.setString(2, String.valueOf(pizzaid));
                    preparedStatementInsertItems.setString(3, amount);
                    preparedStatementInsertItems.executeQuery();
                }
            }

            SessionHelper.abmelden(request);
            request.getSession().setAttribute(ControllerHelper.STATUS, StatusLevel.SUCCESS_BESTELLUNG);
        } catch (SQLException ex) {
            Logger.getLogger(BestellungInsert.class.getName()).log(Level.SEVERE, null, ex);
            LoggerHelper.logBestellung(((Bestellung) request.getSession().getAttribute(ControllerHelper.BESTELLUNG)), "BestellungInsert ");
            request.getSession().setAttribute(ControllerHelper.STATUS, StatusLevel.BESTELLUNG_EXCEPTION);
        }
        response.sendRedirect("index.jsp");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
