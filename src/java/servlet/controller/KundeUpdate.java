package servlet.controller;

import art.dornrose.helper.ControllerHelper;
import art.dornrose.enums.StatusLevel;
import art.dornrose.model.Bestellung;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

/**
 *
 * @author BirkeHeeren
 */
@WebServlet(name = "KundeUpdate", urlPatterns = {"/KundeUpdate"})
public class KundeUpdate extends HttpServlet {

    private DataSource dataSource;
    private String user;
    private String pwd;

    @Override
    public void init(ServletConfig config) throws ServletException {

        super.init(config);

        ServletContext ctx = config.getServletContext();
        user = ctx.getInitParameter("dbUser");
        pwd = ctx.getInitParameter("dbPassword");
        dataSource = (DataSource) ctx.getAttribute("dataSource");
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

         if (request.getSession().getAttribute(ControllerHelper.LOGGEDIN) == null 
                || !(Boolean) request.getSession().getAttribute(ControllerHelper.LOGGEDIN)) {
            response.sendRedirect("index.jsp");
            return;
        }

        if (request.getParameter("email") == null
                || request.getParameter("passwort") == null
                || request.getParameter("vorname") == null
                || request.getParameter("nachname") == null
                || request.getParameter("strasse") == null
                || request.getParameter("ort") == null
                || request.getParameter("telefon") == null
                || ((String) request.getParameter("email")).isBlank()
                || ((String) request.getParameter("passwort")).isBlank()
                || ((String) request.getParameter("vorname")).isBlank()
                || ((String) request.getParameter("nachname")).isBlank()
                || ((String) request.getParameter("strasse")).isBlank()
                || ((String) request.getParameter("ort")).isBlank()
                || ((String) request.getParameter("telefon")).isBlank()) {
            request.getSession().setAttribute(ControllerHelper.STATUS, StatusLevel.KUNDE_DATA_MISSING);
            response.sendRedirect("verwalten.jsp");
            return;
        }

        String passwortNEU = "";
        if (request.getParameter("passwortneu") == null
                || ((String) request.getParameter("passwortneu")).isBlank()) {
        } else {
            passwortNEU = ((String) request.getParameter("passwortneu"));
        }

        String emailNEU = "";
        if (request.getParameter("emailneu") == null
                || ((String) request.getParameter("emailneu")).isBlank()) {
        } else {
            emailNEU = ((String) request.getParameter("emailneu"));
        }

        final String EMAIL = ((String) request.getParameter("email"));
        final String PASSWORT = ((String) request.getParameter("passwort"));
        final String VORNAME = ((String) request.getParameter("vorname"));
        final String NACHNAME = ((String) request.getParameter("nachname"));
        final String STRASSE = ((String) request.getParameter("strasse"));
        final String ORT = ((String) request.getParameter("ort"));
        final String TELEFON = ((String) request.getParameter("telefon"));

        final String ID = ((Bestellung) request.getSession().getAttribute(ControllerHelper.BESTELLUNG)).getKunde().getSessionid();

        final String SQL_SELECT_KONTO = "SELECT* FROM konto WHERE email=? AND passwort=?;";
        final String SQL_UPDATE_KUNDE = "UPDATE kunden SET vorname = ?, nachname = ?, strasse = ?, ort = ?, telefon = ? WHERE sessionid = ?;";
        final String SQL_UPDATE_KUNDE_2 = "UPDATE kunden SET vorname = ?, nachname = ?, strasse = ?, ort = ?, telefon = ?, email = ? WHERE sessionid = ?;";
        final String SQL_DELETE_KONTO = "DELETE FROM konto WHERE email = ?;";
        final String SQL_INSERT_KONTO = "INSERT INTO konto (email, passwort) " + "VALUES ( ?, ?);";
        final String SQL_UPDATE_KONTO = "UPDATE konto SET passwort = ? WHERE email = ?;";

        try (Connection connection = dataSource.getConnection(user, pwd)) {

            PreparedStatement preparedStatementSelectKonto = connection.prepareStatement(SQL_SELECT_KONTO);
            preparedStatementSelectKonto.setString(1, EMAIL);
            preparedStatementSelectKonto.setString(2, PASSWORT);
            ResultSet resultSet = preparedStatementSelectKonto.executeQuery();

            PreparedStatement preparedStatementUpdateKunde = connection.prepareStatement(SQL_UPDATE_KUNDE);
            preparedStatementUpdateKunde.setString(1, VORNAME);
            preparedStatementUpdateKunde.setString(2, NACHNAME);
            preparedStatementUpdateKunde.setString(3, STRASSE);
            preparedStatementUpdateKunde.setString(4, ORT);
            preparedStatementUpdateKunde.setString(5, TELEFON);
            preparedStatementUpdateKunde.setString(6, ID);

            PreparedStatement preparedStatementUpdateKunde2 = connection.prepareStatement(SQL_UPDATE_KUNDE_2);
            preparedStatementUpdateKunde2.setString(1, VORNAME);
            preparedStatementUpdateKunde2.setString(2, NACHNAME);
            preparedStatementUpdateKunde2.setString(3, STRASSE);
            preparedStatementUpdateKunde2.setString(4, ORT);
            preparedStatementUpdateKunde2.setString(5, TELEFON);
            preparedStatementUpdateKunde2.setString(6, emailNEU);
            preparedStatementUpdateKunde2.setString(7, ID);

            PreparedStatement preparedStatementDeleteKonto = connection.prepareStatement(SQL_DELETE_KONTO);
            preparedStatementDeleteKonto.setString(1, EMAIL);

            PreparedStatement preparedStatementInsertKonto = connection.prepareStatement(SQL_INSERT_KONTO);
            preparedStatementInsertKonto.setString(1, EMAIL);
            preparedStatementInsertKonto.setString(2, PASSWORT);

            PreparedStatement preparedStatementInsertKonto2 = connection.prepareStatement(SQL_INSERT_KONTO);
            preparedStatementInsertKonto2.setString(1, emailNEU);
            preparedStatementInsertKonto2.setString(2, PASSWORT);
            
            PreparedStatement preparedStatementInsertKonto3 = connection.prepareStatement(SQL_INSERT_KONTO);
            preparedStatementInsertKonto3.setString(1, emailNEU);
            preparedStatementInsertKonto3.setString(2, passwortNEU);

            PreparedStatement preparedStatementUpdateKonto = connection.prepareStatement(SQL_UPDATE_KONTO);
            preparedStatementUpdateKonto.setString(1, passwortNEU);
            preparedStatementUpdateKonto.setString(2, EMAIL);

            if (!resultSet.first()) {
                SessionHelper.abmelden(request);
                request.getSession().setAttribute("antwortLevel", StatusLevel.KONTO_NOT_FOUND);
                return;
            }

            if (passwortNEU.isBlank() && emailNEU.isBlank()) {
                preparedStatementUpdateKunde.executeUpdate();

            } else if (passwortNEU.isBlank() && !emailNEU.isBlank()) {

                preparedStatementDeleteKonto.executeQuery();
                preparedStatementUpdateKunde2.executeQuery();
                preparedStatementInsertKonto2.executeQuery();
                ((Bestellung) request.getSession().getAttribute(ControllerHelper.BESTELLUNG)).getKunde().setEmail(emailNEU);

            } else if (!passwortNEU.isBlank() && emailNEU.isBlank()) {

                preparedStatementUpdateKunde.executeUpdate();
                preparedStatementUpdateKonto.executeUpdate();

            } else if (!passwortNEU.isBlank() && !emailNEU.isBlank()) {

                preparedStatementDeleteKonto.executeQuery();
                preparedStatementUpdateKunde2.executeUpdate();
                preparedStatementInsertKonto3.executeQuery();
                ((Bestellung) request.getSession().getAttribute(ControllerHelper.BESTELLUNG)).getKunde().setEmail(emailNEU);
            }

            ((Bestellung) request.getSession().getAttribute(ControllerHelper.BESTELLUNG)).getKunde().setVorname(VORNAME);
            ((Bestellung) request.getSession().getAttribute(ControllerHelper.BESTELLUNG)).getKunde().setNachname(NACHNAME);
            ((Bestellung) request.getSession().getAttribute(ControllerHelper.BESTELLUNG)).getKunde().setStrasse(STRASSE);
            ((Bestellung) request.getSession().getAttribute(ControllerHelper.BESTELLUNG)).getKunde().setOrt(ORT);
            ((Bestellung) request.getSession().getAttribute(ControllerHelper.BESTELLUNG)).getKunde().setTelefon(TELEFON);

            request.getSession().setAttribute(ControllerHelper.STATUS, StatusLevel.KUNDE_UPDATED);
        } catch (SQLException ex) {
            SessionHelper.abmelden(request);
            request.getSession().setAttribute(ControllerHelper.STATUS, StatusLevel.KUNDE_SAVE_EXCEPTION);
            Logger.getLogger(KundeUpdate.class.getName()).log(Level.SEVERE, null, ex);
        }
        response.sendRedirect("verwalten.jsp");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
