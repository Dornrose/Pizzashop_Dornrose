package servlet.controller;

import art.dornrose.helper.ControllerHelper;
import art.dornrose.enums.StatusLevel;
import art.dornrose.model.Bestellung;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

/**
 *
 * @author BirkeHeeren
 */
@WebServlet(name = "KundeDelete", urlPatterns = {"/KundeDelete"})
public class KundeDelete extends HttpServlet {

    private DataSource dataSource;
    private String user;
    private String pwd;

    @Override
    public void init(ServletConfig config) throws ServletException {

        super.init(config);

        ServletContext ctx = config.getServletContext();
        user = ctx.getInitParameter("dbUser");
        pwd = ctx.getInitParameter("dbPassword");
        dataSource = (DataSource) ctx.getAttribute("dataSource");
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

         if (request.getSession().getAttribute(ControllerHelper.LOGGEDIN) == null 
                || !(Boolean) request.getSession().getAttribute(ControllerHelper.LOGGEDIN)) {
            response.sendRedirect("index.jsp");
            return;
        }

        if (((Bestellung)request.getSession().getAttribute(ControllerHelper.BESTELLUNG)).getKunde().getEmail().isBlank()
                || request.getParameter("passwortloeschen") == null
                || ((String) request.getParameter("passwortloeschen")).isBlank()) {
            request.getSession().setAttribute(ControllerHelper.STATUS, StatusLevel.KUNDE_EMAIL_OR_PASSWORD_MISSING);
            response.sendRedirect("verwalten.jsp");
            return;
        }

        final String EMAIL = ((Bestellung)request.getSession().getAttribute(ControllerHelper.BESTELLUNG)).getKunde().getEmail();
        final String PASSWORT = ((String) request.getParameter("passwortloeschen"));

        final String SQL_SELECT_KONTO = "SELECT* FROM konto WHERE email=? AND passwort=?;";
        final String SQL_DELETE_KONTO = "DELETE FROM konto WHERE email=?;";
        final String SQL_UPDATE_KUNDE = "UPDATE kunden SET email=NULL WHERE email=?;";

        try (Connection connection = dataSource.getConnection(user, pwd)) {

            PreparedStatement preparedStatementKonto = connection.prepareStatement(SQL_SELECT_KONTO);
            preparedStatementKonto.setString(1, EMAIL);
            preparedStatementKonto.setString(2, PASSWORT);
            ResultSet resultSet = preparedStatementKonto.executeQuery();

            if (!resultSet.first()) {
                SessionHelper.abmelden(request);
                request.getSession().setAttribute(ControllerHelper.STATUS, StatusLevel.KONTO_NOT_FOUND);
                response.sendRedirect("verwalten.jsp");
                return;
            }

            PreparedStatement preparedStatementDelete = connection.prepareStatement(SQL_DELETE_KONTO);
            preparedStatementDelete.setString(1, EMAIL);
            preparedStatementDelete.executeQuery();

            PreparedStatement preparedStatementUpdate = connection.prepareStatement(SQL_UPDATE_KUNDE);
            preparedStatementUpdate.setString(1, EMAIL);
            preparedStatementUpdate.executeUpdate();

            SessionHelper.abmelden(request);
            request.getSession().setAttribute(ControllerHelper.STATUS, StatusLevel.KUNDE_DELETED);

        } catch (SQLException ex) {
            request.getSession().setAttribute(ControllerHelper.STATUS, StatusLevel.KUNDE_DELETE_EXCEPTION);
            Logger.getLogger(KundeDelete.class.getName()).log(Level.SEVERE, null, ex);
        }
        response.sendRedirect("verwalten.jsp");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
