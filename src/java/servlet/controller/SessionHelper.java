package servlet.controller;

import art.dornrose.helper.ControllerHelper;
import art.dornrose.enums.StatusLevel;
import art.dornrose.model.Bestellung;
import art.dornrose.model.Person;
import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author BirkeHeeren
 */
public class SessionHelper {

    private SessionHelper() {

    }

    public static void checkSession(HttpServletRequest request, HttpServletResponse response) throws IOException {
        checkSession(request);
        response.sendRedirect("index.jsp");
    }

    public static void checkSession(HttpServletRequest request) throws IOException {
        if (((request.getSession().getAttribute(ControllerHelper.BESTELLUNG) == null
                || ((Bestellung) request.getSession().getAttribute(ControllerHelper.BESTELLUNG)).getKunde().isEmpty())
                && request.getSession().getAttribute(ControllerHelper.LOGGEDIN) != null
                && (Boolean) request.getSession().getAttribute(ControllerHelper.LOGGEDIN))) {
            abmelden(request);
        } else if ((request.getSession().getAttribute(ControllerHelper.BESTELLUNG) != null
                && !((Bestellung) request.getSession().getAttribute(ControllerHelper.BESTELLUNG)).getKunde().isEmpty()
                && (request.getSession().getAttribute(ControllerHelper.LOGGEDIN) == null
                || !(Boolean) request.getSession().getAttribute(ControllerHelper.LOGGEDIN)))) {
            abmelden(request);
        }
    }

    public static void abmelden(HttpServletRequest request) {
        request.getSession(true).setAttribute(ControllerHelper.BESTELLUNG, new Bestellung());
        request.getSession().setAttribute(ControllerHelper.LOGGEDIN, Boolean.FALSE);
        request.getSession().setAttribute(ControllerHelper.STATUS, StatusLevel.NONE);
    }

    public static void anmelden(HttpServletRequest request, Person person) throws IOException {
        if (request.getSession() == null || request.getSession().getAttribute(ControllerHelper.BESTELLUNG) == null) {
            request.getSession(true).setAttribute(ControllerHelper.BESTELLUNG, new Bestellung());
        }
        ((Bestellung) request.getSession().getAttribute(ControllerHelper.BESTELLUNG)).setKunde(person);
        ((Bestellung) request.getSession().getAttribute(ControllerHelper.BESTELLUNG)).setIpadresse(request.getRemoteAddr());
        request.getSession().setAttribute(ControllerHelper.LOGGEDIN, Boolean.TRUE);
        request.getSession().setAttribute(ControllerHelper.STATUS, StatusLevel.SUCCESS_ANMELDEN);
    }
}
