package servlet.controller.pagespeisekarte;

import art.dornrose.enums.StatusLevel;
import art.dornrose.model.Bestellung;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import art.dornrose.helper.ControllerHelper;

/**
 *
 * @author BirkeHeeren
 */
@WebServlet(name = "PizzaBack", urlPatterns = {"/PizzaBack"})
public class PizzaBack extends HttpServlet {

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if (request.getSession() == null || request.getSession().getAttribute(ControllerHelper.BESTELLUNG) == null) {
            request.getSession(true).setAttribute(ControllerHelper.BESTELLUNG, new Bestellung());
        }
        request.getSession().setAttribute(ControllerHelper.STATUS, StatusLevel.WARENKOPRB_LEER);
        response.sendRedirect("index.jsp");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
