package servlet.controller.pagespeisekarte;

import art.dornrose.enums.StatusLevel;
import art.dornrose.model.Bestellung;
import java.io.IOException;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import art.dornrose.helper.ControllerHelper;

/**
 *
 * @author BirkeHeeren
 */
@WebServlet(name = "Minus", urlPatterns = {"/Minus"})
public class Minus extends HttpServlet {

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

       

        ((Bestellung) request.getSession().getAttribute("bestellung")).getBestellungMap().entrySet().stream()
                .map(Map.Entry::getValue)
                .filter(pizza -> pizza.getButtonMinusName().equals(request.getParameterMap().keySet().stream()
                                                                         .filter(name -> request.getParameter(name) != null)
                                                                          .findFirst().get()))
                .findFirst().get().minusAmount();
        
  
        
        if (((Bestellung) request.getSession().getAttribute("bestellung")).getTotalNumberOfPizzasChosen() > 0) {
            request.getSession().setAttribute(ControllerHelper.STATUS, StatusLevel.WARNKORB_VOLL);
        } else {
            request.getSession().setAttribute(ControllerHelper.STATUS, StatusLevel.WARENKOPRB_LEER);
        }

        response.sendRedirect("index.jsp");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
