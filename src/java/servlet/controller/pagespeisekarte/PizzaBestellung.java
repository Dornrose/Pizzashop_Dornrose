package servlet.controller.pagespeisekarte;

import art.dornrose.enums.StatusLevel;
import art.dornrose.model.Bestellung;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import art.dornrose.helper.ControllerHelper;
import servlet.controller.SessionHelper;

/**
 *
 * @author BirkeHeeren
 */
@WebServlet(name = "PizzaBestellung", urlPatterns = {"/PizzaBestellung"})
public class PizzaBestellung extends HttpServlet {

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        SessionHelper.checkSession(request, response);

        if (((Bestellung) request.getSession().getAttribute("bestellung")).getTotalNumberOfPizzasChosen() == 0) {

            request.getSession().setAttribute(ControllerHelper.STATUS, StatusLevel.WARENKOPRB_LEER);
        }
        else
        {
            request.getSession().setAttribute(ControllerHelper.STATUS, StatusLevel.WARNKORB_VOLL);
        }
        
        response.sendRedirect("bestellung.jsp");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
