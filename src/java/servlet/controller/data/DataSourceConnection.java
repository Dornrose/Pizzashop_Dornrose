/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet.controller.data;

import art.dornrose.model.Pizza;
import java.util.HashMap;
import java.util.Map;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Blob;

import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.sql.DataSource;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 *
 * @author BirkeHeeren
 */
@WebListener
public class DataSourceConnection implements ServletContextListener {
    
    private static Map<Integer, Pizza> pizzaMap = new HashMap<>();
    private static final String SQL = "SELECT* FROM pizzas;";
    private Connection connection;
    private static final double TAX = 0.07;
    
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        ServletContext ctx = servletContextEvent.getServletContext();

        //initialize DB Connection
        String adress = ctx.getInitParameter("address");
        String user = ctx.getInitParameter("dbUser");
        String pwd = ctx.getInitParameter("dbPassword");
        
        
        
        try {
            Context context = new InitialContext();
            DataSource dataSource = (DataSource)context.lookup(adress);
            
            ctx.setAttribute("dataSource", dataSource); // kann von überall aufgerufen werden.
            
            connection = dataSource.getConnection(user, pwd);        
            
            PreparedStatement prepStat = connection.prepareStatement(SQL);
            ResultSet resultSet = prepStat.executeQuery();
            
            
            
            while (resultSet.next()) {
                
                Pizza pizza = new Pizza();
                pizza.setId(resultSet.getInt("id"));
                pizza.setName(resultSet.getString("name"));
                pizza.setPrice(resultSet.getDouble("netto_price"));
                pizza.setImage("pizza"+pizza.getId()+".jpg");
                
                pizzaMap.put(pizza.getId(), pizza);
                
                Blob blob = resultSet.getBlob("image");
                ctx.setAttribute(pizza.getImage(), blob.getBytes(1, (int) blob.length()));
            }
            
            connection.close();
            
        } catch (SQLException | NamingException ex) {
            Logger.getLogger(DataSourceConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally
        {
            if(connection != null)
            {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataSource.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        
    }
    
    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        
    }
    
    public static double getTAX() {
        return TAX;
    }
        
    public static Map<Integer, Pizza> deepClonePizzaMap() {
        return pizzaMap.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey, pizza -> new Pizza(pizza.getValue())));
    }
    
    
    
}
