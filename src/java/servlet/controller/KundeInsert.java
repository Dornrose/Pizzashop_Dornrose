package servlet.controller;

import art.dornrose.helper.ControllerHelper;
import art.dornrose.enums.StatusLevel;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

/**
 *
 * @author BirkeHeeren
 */
@WebServlet(name = "KundeInsert", urlPatterns = {"/KundeInsert"})
public class KundeInsert extends HttpServlet {

    private DataSource dataSource;
    private String user;
    private String pwd;

    @Override
    public void init(ServletConfig config) throws ServletException {

        super.init(config);

        ServletContext ctx = config.getServletContext();
        user = ctx.getInitParameter("dbUser");
        pwd = ctx.getInitParameter("dbPassword");
        dataSource = (DataSource) ctx.getAttribute("dataSource");
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        if (request.getSession().getAttribute(ControllerHelper.LOGGEDIN) != null 
                && (Boolean) request.getSession().getAttribute(ControllerHelper.LOGGEDIN)) {
            response.sendRedirect("index.jsp");
            return;
        }

        if (request.getParameter("email") == null
                || request.getParameter("passwort") == null
                || request.getParameter("vorname") == null
                || request.getParameter("nachname") == null
                || request.getParameter("strasse") == null
                || request.getParameter("ort") == null
                || request.getParameter("telefon") == null
                || ((String) request.getParameter("email")).isBlank()
                || ((String) request.getParameter("passwort")).isBlank()
                || ((String) request.getParameter("vorname")).isBlank()
                || ((String) request.getParameter("nachname")).isBlank()
                || ((String) request.getParameter("strasse")).isBlank()
                || ((String) request.getParameter("ort")).isBlank()
                || ((String) request.getParameter("telefon")).isBlank()) {
            request.getSession().setAttribute(ControllerHelper.STATUS, StatusLevel.KUNDE_DATA_MISSING);
            response.sendRedirect("index.jsp");
            return;
        }

        final String ID = UUID.randomUUID().toString();
        final String EMAIL = ((String) request.getParameter("email"));
        final String PASSWORT = ((String) request.getParameter("passwort"));
        final String VORNAME = ((String) request.getParameter("vorname"));
        final String NACHNAME = ((String) request.getParameter("nachname"));
        final String STRASSE = ((String) request.getParameter("strasse"));
        final String ORT = ((String) request.getParameter("ort"));
        final String TELEFON = ((String) request.getParameter("telefon"));

        final String SQL_SELECT_KUNDE = "SELECT* FROM kunden WHERE email=?;";
        final String SQL_SELECT_KONTO = "SELECT* FROM konto WHERE email=? AND passwort=?;";
        final String SQL_INSERT_KUNDE = "INSERT INTO kunden (sessionid, vorname, nachname, strasse, ort, telefon, email) "
                + "VALUES ( ?, ?, ?, ?, ?, ?, ?);";
        final String SQL_INSERT_KONTO = "INSERT INTO konto (email, passwort) VALUES (?, ?)";

        try (Connection connection = dataSource.getConnection(user, pwd)) {

            PreparedStatement preparedStatementSelectKunde = connection.prepareStatement(SQL_SELECT_KUNDE);
            preparedStatementSelectKunde.setString(1, EMAIL);
            ResultSet resultSetSelectKunde = preparedStatementSelectKunde.executeQuery();

            if (resultSetSelectKunde.first()) {
                request.getSession().setAttribute(ControllerHelper.STATUS, StatusLevel.KUNDE_MULTIPLE);
                 response.sendRedirect("index.jsp");
                return;
            }

            PreparedStatement preparedStatementSelectKonto = connection.prepareStatement(SQL_SELECT_KONTO);
            preparedStatementSelectKonto.setString(1, EMAIL);
            preparedStatementSelectKonto.setString(2, PASSWORT);
            ResultSet resultSetSelectKonto = preparedStatementSelectKonto.executeQuery();

            if (resultSetSelectKonto.first()) {
                request.getSession().setAttribute(ControllerHelper.STATUS, StatusLevel.DATABASE_INKONSISTENT);
                Logger.getLogger(KundeInsert.class.getName()).log(Level.WARNING, "WARNING DATABASE LOST KONTO with email {0}", EMAIL);
                 response.sendRedirect("index.jsp");
                return;
            }

            PreparedStatement preparedStatementInsertKunde = connection.prepareStatement(SQL_INSERT_KUNDE);
            preparedStatementInsertKunde.setString(1, ID);
            preparedStatementInsertKunde.setString(2, VORNAME);
            preparedStatementInsertKunde.setString(3, NACHNAME);
            preparedStatementInsertKunde.setString(4, STRASSE);
            preparedStatementInsertKunde.setString(5, ORT);
            preparedStatementInsertKunde.setString(6, TELEFON);
            preparedStatementInsertKunde.setString(7, EMAIL);
            preparedStatementInsertKunde.executeQuery();

            PreparedStatement preparedStatementInsertKonto = connection.prepareStatement(SQL_INSERT_KONTO);
            preparedStatementInsertKonto.setString(1, EMAIL);
            preparedStatementInsertKonto.setString(2, PASSWORT);
            preparedStatementInsertKonto.executeQuery();

            request.getSession().setAttribute(ControllerHelper.STATUS, StatusLevel.SUCCESS_REGISTRATION);

        } catch (SQLException ex) {
            request.getSession().setAttribute(ControllerHelper.STATUS, StatusLevel.REGISTRATION_EXCEPTION);
            Logger.getLogger(KundeInsert.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        response.sendRedirect("index.jsp");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
