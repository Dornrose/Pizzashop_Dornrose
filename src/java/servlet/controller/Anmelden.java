package servlet.controller;

import art.dornrose.helper.ControllerHelper;
import art.dornrose.enums.StatusLevel;
import art.dornrose.model.Person;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author BirkeHeeren
 */
@WebServlet(name = "Anmelden", urlPatterns = {"/Anmelden"})
public class Anmelden extends HttpServlet {

    private final String REDIRECT = "index.jsp";
    
    private DataSource dataSource;
    private String user;
    private String pwd;

    @Override
    public void init(ServletConfig config) throws ServletException {

        super.init(config);

        ServletContext ctx = config.getServletContext();
        user = ctx.getInitParameter("dbUser");
        pwd = ctx.getInitParameter("dbPassword");
        dataSource = (DataSource) ctx.getAttribute("dataSource");
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        if (request.getSession().getAttribute(ControllerHelper.LOGGEDIN) != null 
                && (Boolean) request.getSession().getAttribute(ControllerHelper.LOGGEDIN)) {
             response.sendRedirect(REDIRECT);
            return;
        }

        if (request.getParameter("email") == null
                || request.getParameter("passwort") == null
                || ((String) request.getParameter("email")).isBlank()
                || ((String) request.getParameter("passwort")).isBlank()) {
            SessionHelper.abmelden(request);
            request.getSession().setAttribute(ControllerHelper.STATUS, StatusLevel.KUNDE_DATA_MISSING);
             response.sendRedirect(REDIRECT);
            return;
        }

        final String EMAIL = ((String) request.getParameter("email"));
        final String PASSWORT = ((String) request.getParameter("passwort"));

        final String SQL_SELECT_KONTO = "SELECT* FROM konto WHERE email=? AND passwort=?;";
        final String SQL_SELECT_KUNDE = "SELECT* FROM kunden WHERE email=?;";

        try (Connection connection = dataSource.getConnection(user, pwd)) {

            PreparedStatement preparedStatementSelectKonto = connection.prepareStatement(SQL_SELECT_KONTO);
            preparedStatementSelectKonto.setString(1, EMAIL);
            preparedStatementSelectKonto.setString(2, PASSWORT);
            ResultSet resultSet = preparedStatementSelectKonto.executeQuery();

            if (!resultSet.first()) {
                SessionHelper.abmelden(request);
                request.getSession().setAttribute(ControllerHelper.STATUS, StatusLevel.KONTO_NOT_FOUND);
                 response.sendRedirect(REDIRECT);
                return;
            }

            PreparedStatement preparedStatementSelectKunde = connection.prepareStatement(SQL_SELECT_KUNDE);
            preparedStatementSelectKunde.setString(1, EMAIL);
            ResultSet resultSetSelectKunde = preparedStatementSelectKunde.executeQuery();

            if (!resultSetSelectKunde.first()) {
                request.getSession().setAttribute(ControllerHelper.STATUS, StatusLevel.KUNDE_NOT_FOUND);
                 response.sendRedirect(REDIRECT);
                return;
            }

            Person person = new Person();
            person.setSessionid(resultSetSelectKunde.getString("sessionid"));
            person.setEmail(resultSetSelectKunde.getString("email"));
            person.setVorname(resultSetSelectKunde.getString("vorname"));
            person.setNachname(resultSetSelectKunde.getString("nachname"));
            person.setStrasse(resultSetSelectKunde.getString("strasse"));
            person.setOrt(resultSetSelectKunde.getString("ort"));
            person.setTelefon(resultSetSelectKunde.getString("telefon"));
            
            SessionHelper.anmelden(request, person);

        } catch (SQLException ex) {
            request.getSession().setAttribute(ControllerHelper.STATUS, StatusLevel.ANMELDEN_EXCEPTION);
            Logger.getLogger(Anmelden.class.getName()).log(Level.SEVERE, null, ex);
        }
            response.sendRedirect(REDIRECT);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
