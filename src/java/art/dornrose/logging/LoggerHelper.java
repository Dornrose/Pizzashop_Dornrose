package art.dornrose.logging;

import art.dornrose.model.Bestellung;
import art.dornrose.model.Person;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author BirkeHeeren
 */
public class LoggerHelper {

    private static void logKunde(Person kunde) {
        Logger.getLogger(LoggerHelper.class.getName()).log(Level.INFO, "Kunde");
        Logger.getLogger(LoggerHelper.class.getName()).log(Level.INFO, "-----");
        Logger.getLogger(LoggerHelper.class.getName()).log(Level.INFO, "ID: {0}", kunde.getSessionid());
        Logger.getLogger(LoggerHelper.class.getName()).log(Level.INFO, "Vorname: {0}", kunde.getVorname());
        Logger.getLogger(LoggerHelper.class.getName()).log(Level.INFO, "Nachname: {0}", kunde.getNachname());
        Logger.getLogger(LoggerHelper.class.getName()).log(Level.INFO, "Strasse: {0}", kunde.getStrasse());
        Logger.getLogger(LoggerHelper.class.getName()).log(Level.INFO, "Ort: {0}", kunde.getOrt());
        Logger.getLogger(LoggerHelper.class.getName()).log(Level.INFO, "Telefon: {0}", kunde.getTelefon());
        Logger.getLogger(LoggerHelper.class.getName()).log(Level.INFO, "Email: {0}", kunde.getEmail());

    }
    
    private LoggerHelper()
    {
        
    }
    
    public static void logBestellung(Bestellung bestellung, String info)
    {
       
        Logger.getLogger(LoggerHelper.class.getName()).log(Level.INFO, "\n===== "+info+" =====");
        Logger.getLogger(LoggerHelper.class.getName()).log(Level.INFO, "Bestellung");
        Logger.getLogger(LoggerHelper.class.getName()).log(Level.INFO, "----------");
        Logger.getLogger(LoggerHelper.class.getName()).log(Level.INFO, "Anzahl Pizzen: {0}", bestellung.getTotalNumberOfPizzasChosen());
        Logger.getLogger(LoggerHelper.class.getName()).log(Level.INFO, "Endpreis: {0}", bestellung.calculateGesamtpreisBrutto());
        Logger.getLogger(LoggerHelper.class.getName()).log(Level.INFO, "----------");
        LoggerHelper.logKunde(bestellung.getKunde());
        Logger.getLogger(LoggerHelper.class.getName()).log(Level.INFO, "==========");
    }
}
