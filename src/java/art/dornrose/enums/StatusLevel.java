package art.dornrose.enums;

/**
 *
 * @author BirkeHeeren
 */
public enum StatusLevel {
    
    NONE("Wir wünschen Ihnen einen guten Einkauf."),
    SUCCESS_ANMELDEN("Wir wünschen Ihnen einen guten Einkauf."),
    ANMELDEN_EXCEPTION("Bei der Anmeldung hat es einen Fehler gegeben."),
    KUNDE_NOT_FOUND("Der Kunde konnte nicht gefunden werden."),
    KUNDE_SAVE_EXCEPTION("Der Kunde konnte nicht gespeichert werden."),
    KUNDE_DATA_MISSING("Es fehlen noch Angaben. Haben Sie das Passwort eingegeben?"),
    KUNDE_EMAIL_OR_PASSWORD_MISSING("Email und/oder Passwort fehlen."),
    KUNDE_MULTIPLE("Dieses Konto gibt es schon."),
    KONTO_NOT_FOUND("Das Konto konnte nicht gefunden werden."),
    REGISTRATION_EXCEPTION("Bei der Registrierung hat es einen Fehler gegeben."),
    SUCCESS_REGISTRATION("Die Registrierung war erfolgreich."),
    SUCCESS_BESTELLUNG("Die Bestellung war erfolgreich."),
    KUNDE_UPDATED("Die Kundendaten wurden geändert."),
    KUNDE_DELETED("Das Konto wurde gelöscht."),
    KUNDE_DELETE_EXCEPTION("Beim Löschen des Kontos hat es einen Fehler gegeben."),
    BESTELLUNG_EXCEPTION("Bei der Bestellung hat es einen Fehler gegeben."),
    WARENKOPRB_LEER("Ihr Warenkorb ist leer"),
    WARNKORB_VOLL("Sie haben Pizzas im Warenkorb"),
    DATABASE_INKONSISTENT("Es gibt einen Fehler in der Datenbank. Bitte schreiben Sie dem Kundendienst.")
    ;
    
    private final String meldung;
    
    StatusLevel(String meldung)
    {
        this.meldung = meldung;
    }

    public String getMeldung() {
        return meldung;
    }

}
