package art.dornrose.model;

/**
 *
 * @author BirkeHeeren
 */
public class Person {

    private String vorname = "";
    private String nachname = "";
    private String strasse = "";
    private String ort = "";
    private String telefon = "";
    private String sessionid = "";
    private String email = "";

    public boolean isEmpty() {
        return vorname.isBlank() || nachname.isBlank() || strasse.isBlank() || ort.isBlank() || telefon.isBlank() || sessionid.isBlank() || email.isBlank();
    }

    public String getVorname() {
        return vorname;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    public String getNachname() {
        return nachname;
    }

    public void setNachname(String nachname) {
        this.nachname = nachname;
    }

    public String getStrasse() {
        return strasse;
    }

    public void setStrasse(String strasse) {
        this.strasse = strasse;
    }

    public String getOrt() {
        return ort;
    }

    public void setOrt(String ort) {
        this.ort = ort;
    }

    public String getTelefon() {
        return telefon;
    }

    public void setTelefon(String telefon) {
        this.telefon = telefon;
    }

    public String getSessionid() {
        return sessionid;
    }

    public void setSessionid(String sessionid) {
        this.sessionid = sessionid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
