package art.dornrose.model;

import servlet.controller.data.DataSourceConnection;
import java.util.Map;
import java.util.UUID;

/**
 *
 * @author BirkeHeeren
 */
public class Bestellung {

    private final double BRUTTO = 1.0 + DataSourceConnection.getTAX();
    private final double DELIVERY = 0.5;

    private Map<Integer, Pizza> bestellungMap;
    private UUID uuid;
    private Person kunde;
    private String ipadresse;

    public Bestellung() {
        bestellungMap = DataSourceConnection.deepClonePizzaMap();
        kunde = new Person();
    }
    
     public Bestellung(Person kunde) {
        bestellungMap = DataSourceConnection.deepClonePizzaMap();
        kunde = kunde;
    }

    public double calculateGesamtpreisBrutto() {
        return calculateGesamtpreisNetto() * BRUTTO;
    }

    public double calculateGesamtpreisNetto() {
        double preis = 0;
        for (Integer pizzaId : bestellungMap.keySet()) {
            Pizza pizza = bestellungMap.get(pizzaId);
            preis += pizza.getAmount() * pizza.getPrice();
        }
        return preis;
    }

    public double calculateTax() {
        return calculateGesamtpreisNetto() * DataSourceConnection.getTAX();
    }

    public double calculateTotal() {
        return calculateGesamtpreisBrutto() + getDelivery();
    }

    public double getDelivery() {
        if (calculateGesamtpreisBrutto() > 0) {
            return DELIVERY;
        }
        return 0.0;
    }

    public int getTotalNumberOfPizzasChosen() {
        return bestellungMap.entrySet().stream().map(Map.Entry::getValue).map(pizza -> pizza.getAmount()).reduce(0, Integer::sum);
    }

    public Map<Integer, Pizza> getBestellungMap() {
        return bestellungMap;
    }

    public void setBestellungMap(Map<Integer, Pizza> bestellungMap) {
        this.bestellungMap = bestellungMap;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public Person getKunde() {
        return kunde;
    }

    public void setKunde(Person kunde) {
        this.kunde = kunde;
    }

    public String getIpadresse() {
        return ipadresse;
    }

    public void setIpadresse(String ipadresse) {
        this.ipadresse = ipadresse;
    }

}
