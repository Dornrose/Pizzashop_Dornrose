package art.dornrose.model;

import servlet.controller.data.DataSourceConnection;

/**
 *
 * @author BirkeHeeren
 */
public class Pizza {

    private int id;
    private String name;
    private double price;
    private String image;
    private int amount;

    public Pizza ()
    {
        
    }
    
    public Pizza(Pizza pizza)
    {
        id = pizza.getId();
        name = pizza.getName();
        price = pizza.getPrice();
        image = pizza.getImage();
        amount = pizza.getAmount();
    }
    
    public void addAmount() {
        amount++;
    }

    public void minusAmount() {
        if (amount == 0) {
            return;
        }
        amount--;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }
    
    public double getBruttoPrice() {
        return price*(1+DataSourceConnection.getTAX());
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }    

    public String getButtonPlusName() {
        return "buttonplus" + id;
    }

    public String getButtonMinusName() {
        return "buttonminus" + id;
    }

    public String getInputName() {
        return "input" + id;
    }
}
