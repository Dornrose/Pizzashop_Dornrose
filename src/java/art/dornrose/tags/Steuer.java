/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package art.dornrose.tags;

import java.io.IOException;
import java.text.NumberFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.BodyTagSupport;
import static javax.servlet.jsp.tagext.Tag.SKIP_BODY;

/**
 *
 * @author BirkeHeeren
 */
public class Steuer extends BodyTagSupport {

    private double value;
    NumberFormat formatter = NumberFormat.getPercentInstance();

    @Override
    public int doStartTag() {
        try {

            JspWriter out = pageContext.getOut();
            out.print(formatter.format(value));

        } catch (IOException ex) {
            Logger.getLogger(Steuer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return SKIP_BODY;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

}
