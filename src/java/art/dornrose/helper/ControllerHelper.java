package art.dornrose.helper;

/**
 *
 * @author BirkeHeeren
 */
public class ControllerHelper {

    public static final String LOGGEDIN = "loggedIn";
    public static final String STATUS = "antwortLevel";
    public static final String BESTELLUNG = "bestellung";

    public static String getLOGGEDIN() {
        return LOGGEDIN;
    }

    public static String getSTATUS() {
        return STATUS;
    }

    public static String getBESTELLUNG() {
        return BESTELLUNG;
    }

}
