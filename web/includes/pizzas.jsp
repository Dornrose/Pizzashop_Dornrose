<%-- 
    Document   : pizzas
    Created on : 02.03.2021, 20:17:48
    Author     : BirkeHeeren
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="dr" uri="dornrosetags" %>

<%@page import="art.dornrose.helper.ControllerHelper"%>
<%@page import="art.dornrose.model.Bestellung"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<jsp:useBean id="bestellung" class="art.dornrose.model.Bestellung" scope="session" />
<jsp:useBean id="helper" class="art.dornrose.helper.ControllerHelper"/>

<div id="pizzas">

            <c:forEach items="${bestellung.getBestellungMap().keySet()}" var="pizzaId">
                <c:set var="pizza" value="${bestellung.getBestellungMap().get(pizzaId)}" />

                <div class="pizzarow"> 
                    <div class="pizzacol"><img src="images/<c:out value="${pizza.getImage()}" />" alt="<c:out value="${pizza.getName()}" />"/></div>
                    <br>
                    <div class="pizzacol">
                        <input  name="<c:out value="${pizza.getInputName()}" />" disabled class="input" value="<c:out value="${pizza.getAmount()}" />">

                        <form class="formformat" action="Plus" method="post">
                            <input type="hidden" name="<c:out value="${pizza.getButtonPlusName()}" />" value="<c:out value="${pizza.getButtonPlusName()}" />" />
                            <button type="submit">+</button>
                        </form>

                        <form class="formformat" action="Minus" method="post">
                            <input type="hidden" name="<c:out value="${pizza.getButtonMinusName()}" />" value="<c:out value="${pizza.getButtonMinusName()}" />" />
                            <button type="submit">-</button>
                        </form>

                    </div>
                    <br>
                    <div class="pizzacol"><label class="pizzaname"><c:out value="${pizza.getName()}" /></label></div>
                    <div class="pizzacol"><label class="pizzaprice"><dr:preis value="${pizza.getBruttoPrice()}" /></label></div>
                    <br>
                    <div>

                    </div>
                </div>
            </c:forEach>

        </div>