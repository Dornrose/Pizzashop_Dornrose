<%-- 
    Document   : header
    Created on : 01.03.2021, 17:33:11
    Author     : BirkeHeeren
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@page import="art.dornrose.helper.ControllerHelper"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<jsp:useBean id="helper" class="art.dornrose.helper.ControllerHelper"/>

<header>
    <c:choose>
        <c:when test="${fn:endsWith(pageContext.request.requestURI, 'verwalten.jsp') 
                && session ne null 
                && session.getArttribute(helper.getLOGGEDIN) ne null
                && session.getArttribute(helper.getLOGGEDIN) ne false}">
            <div class="personheaderright">
                <form action="KundeDelete" method="post"> 
                    <button name="loeschen" class="kundespeichern">Konto lösschen</button>&#160;
                    <label>Passwort</label>&#160;
                    <input name="passwortloeschen" type="password">
                </form>
            </div>
        </c:when>
        <c:when test="${fn:endsWith(pageContext.request.requestURI, 'registrierung.jsp')}">


        </c:when>
        <c:otherwise>
            <c:if test="${sessionScope[helper.getLOGGEDIN()] eq null || sessionScope[helper.getLOGGEDIN()] eq false}">

                <div class="personheaderright">
                    <form action="<%=request.getContextPath()%>/registrierung.jsp" method="post"> 
                        <button name="registrieren" class="kundespeichern">registrieren</button>
                    </form>
                </div>

                <div class="personheaderleft">
                    <form action="Anmelden" method="post">
                        Email  
                        <input name="email">
                        Passwort
                        <input name="passwort" type="password">

                        <button class="kundeanmelden" type="submit">anmelden</button>
                    </form>
                </div>

            </c:if>
            <c:if test="${sessionScope[helper.getLOGGEDIN()] ne null && sessionScope[helper.getLOGGEDIN()] eq true}">

                <div class="personheaderright">
                    <form action="<%=request.getContextPath()%>/verwalten.jsp" method="post"> 
                        <button name="verwalten" class="kundespeichern">Konto verwalten</button>
                    </form>
                </div>

            </c:if>
        </c:otherwise>
    </c:choose>
</header>
