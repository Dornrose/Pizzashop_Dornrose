<%-- 
    Document   : warenkorbinner
    Created on : 01.03.2021, 23:49:41
    Author     : BirkeHeeren
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="dr" uri="dornrosetags" %>

<%@page import="servlet.controller.data.DataSourceConnection"%>
<%@page import="art.dornrose.model.Bestellung"%>
<%@page import="art.dornrose.model.Pizza"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<jsp:useBean id="pizzaMap" class="servlet.controller.data.DataSourceConnection" scope="application" />
<jsp:useBean id="bestellung" class="art.dornrose.model.Bestellung" scope="session" />

<c:forEach items="${bestellung.getBestellungMap().keySet()}" var="pizzaId">
    <c:set var="pizza" value="${bestellung.getBestellungMap().get(pizzaId)}" />
    <c:if test="${pizza.getAmount() > 0}">
        <tr>
            <td class="warenkorbpizza"><c:out value="${pizza.getName()}" />
        <td class="warenkorbpizza"><c:out value="${pizza.getAmount()}" />
        
    </c:if>    
</c:forEach>

<tr>
    <td class="leichte-linie"><label>Gesamtpreis, brutto</label><td class="preis"><label id="brutto">
            <dr:preis value="${bestellung.calculateGesamtpreisBrutto()}" /></label>
<tr>
    <td class="leichte-linie"><label>Gesamtpreis, netto</label><td class="preis"><label id="netto">
            <dr:preis value="${bestellung.calculateGesamtpreisNetto()}" /></label>
<tr>
    <td  class="leichte-linie"><label>Mehrwertsteuer 
            <dr:steuer value="${pizzaMap.getTAX()}" /></label>
    <td class="preis"><label id="steuer">
            <dr:preis value="${bestellung.calculateTax()}" /></label>
<tr>
    <td class="leichte-linie"><label>Lieferung</label><td class="preis-linie"><label>
            
            <dr:preis value="${bestellung.getDelivery()}" /></label>
<tr>
    <td class="keine-linie"><label>Endpreis</label><td class="preis-linie"><label id="total">
            <dr:preis value="${bestellung.calculateTotal()}" /></label>
<tr>
    <td><td class="unterlinie"> 
        
        