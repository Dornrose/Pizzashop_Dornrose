<%-- 
    Document   : konto.jsp
    Created on : 26.02.2021, 04:39:26
    Author     : BirkeHeeren
--%>

<%@ taglib prefix="dr" uri="dornrosetags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@page import="art.dornrose.enums.StatusLevel"%>
<%@page import="servlet.controller.SessionHelper"%>
<%@page import="art.dornrose.helper.ControllerHelper"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<jsp:useBean id="helper" class="art.dornrose.helper.ControllerHelper"/>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Kundenkonto registrieren</title>
        <link rel="stylesheet" href="css/main.css">
    </head>
    <body>

        <jsp:include page="includes/ensureConsistentState.jsp" />

        <jsp:include page="includes/header.jsp" />
        <jsp:include page="includes/headeranmeldung.jsp" />
        <jsp:include page="includes/meldungen.jsp" />


        <div id="headerkonto">
            <img class="tomate" src="img/tomate.png" alt="Tomate"/>

            <c:choose>
                <c:when test="${sessionScope[helper.getLOGGEDIN()] eq null || sessionScope[helper.getLOGGEDIN()] eq false}">


                    <form action="KundeInsert" method="post"> 
                        <table class="person">
                            <colgroup>
                                <col id="headercol">
                                <col id="inputcol">
                                <col id="buttoncol">
                                <col id="antwortcol">
                            </colgroup> 

                            <tr>
                                <td>Email</td>   
                                <td><input name="email"></td>
                                <td></td>   
                                <td></td>   
                            </tr>


                            <tr>
                                <td>Passwort</td>   

                                <td><input name="passwort" type="password"></td>
                                <td><button name="registieren" class="kundespeichern">registrieren</button></td>   
                                <td></td> 
                            </tr>


                            <tr>
                                <td>Vorname</td>
                                <td><input name="vorname"></td>
                                <td></td> 
                                <td></td> 
                            </tr>
                            <tr>
                                <td>Nachname</td>  
                                <td><input name="nachname"></td>
                                <td></td> 
                                <td></td> 
                            </tr>
                            <tr>
                                <td>Straße Hausnummer</td>  
                                <td><input name="strasse"></td>
                                <td></td> 
                                <td></td> 
                            </tr>
                            <tr>
                                <td>PLZ Ort</td>  
                                <td><input name="ort"></td>
                                <td></td> 
                                <td></td> 
                            </tr>
                            <tr>
                                <td>Telefon</td> 
                                <td><input name="telefon"></td>
                                <td></td> 
                                <td></td> 
                            </tr>
                            <tr>


                        </table>

                    </form>

                </c:when>
                <c:otherwise>
                  
                </c:otherwise>
            </c:choose>

        </div> 
        <jsp:include page="includes/footer.jsp" />
    </body>
</html>
