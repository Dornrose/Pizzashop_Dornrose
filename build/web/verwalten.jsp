<%-- 
    Document   : konto.jsp
    Created on : 26.02.2021, 04:39:26
    Author     : BirkeHeeren
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="dr" uri="dornrosetags" %>

<%@page import="art.dornrose.model.Bestellung"%>
<%@page import="art.dornrose.enums.StatusLevel"%>
<%@page import="servlet.controller.SessionHelper"%>
<%@page import="art.dornrose.helper.ControllerHelper"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<jsp:useBean id="helper" class="art.dornrose.helper.ControllerHelper"/>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Kundenkonto verwalten</title>
        <link rel="stylesheet" href="css/main.css">
    </head>
    <body>

        <jsp:include page="includes/header.jsp" />
        <jsp:include page="includes/headeranmeldung.jsp" />
        <img class="tomate" src="img/tomate.png" alt="Tomate"/>     
        <jsp:include page="includes/meldungen.jsp" />

        <c:choose>
            <c:when test="${sessionScope[helper.getLOGGEDIN()] ne null && sessionScope[helper.getLOGGEDIN()] eq true}">
                <form action="KundeUpdate" method="post"> 
                    <table class="person">
                        <colgroup>
                            <col id="headercol">
                            <col id="inputcol">
                            <col id="buttoncol">
                            <col id="antwortcol">
                        </colgroup> 
                         <tr>
                             <td colspan="4"><h2>Konto verwalten</h2></td>     
                        </tr>
                        <tr>
                            <td>Email</td>   
                            <td><input name="email" value="<%=((Bestellung) session.getAttribute(ControllerHelper.BESTELLUNG)).getKunde().getEmail()%>"></td>
                            <td></td>   
                            <td></td>   
                        </tr>
                        <tr>
                            <td>Email neu</td>   
                            <td><input name="emailneu"></td>
                            <td></td>   
                            <td></td>   
                        </tr>
                        <tr>
                            <td>Passwort</td>   

                            <td><input name="passwort" type="password"></td>
                            <td><button name="speichern" class="kundespeichern">speichern</button></td>   
                            <td></td> 
                        </tr>
                        <tr>
                            <td>Passwort neu</td>   
                            <td><input name="passwortneu" type="password"></td>
                            <td></td>   
                            <td></td>   
                        </tr>
                        <tr>
                            <td>Vorname</td>
                            <td><input name="vorname" value="<%=((Bestellung) session.getAttribute(ControllerHelper.BESTELLUNG)).getKunde().getVorname()%>"></td>
                            <td></td> 
                            <td></td> 
                        </tr>
                        <tr>
                            <td>Nachname</td>  
                            <td><input name="nachname" value="<%=((Bestellung) session.getAttribute(ControllerHelper.BESTELLUNG)).getKunde().getNachname()%>"></td>
                            <td></td> 
                            <td></td> 
                        </tr>
                        <tr>
                            <td>Straße Hausnummer</td>  
                            <td><input name="strasse" value="<%=((Bestellung) session.getAttribute(ControllerHelper.BESTELLUNG)).getKunde().getStrasse()%>"></td>
                            <td></td> 
                            <td></td> 
                        </tr>
                        <tr>
                            <td>PLZ Ort</td>  
                            <td><input name="ort" value="<%=((Bestellung) session.getAttribute(ControllerHelper.BESTELLUNG)).getKunde().getOrt()%>"></td>
                            <td></td> 
                            <td></td> 
                        </tr>
                        <tr>
                            <td>Telefon</td> 
                            <td><input name="telefon" value="<%=((Bestellung) session.getAttribute(ControllerHelper.BESTELLUNG)).getKunde().getTelefon()%>"></td>
                            <td></td> 
                            <td></td> 
                        </tr>
                        <tr>
                    </table>
                </form>
            </c:when>
            <c:otherwise>

            </c:otherwise>
        </c:choose>

        <jsp:include page="includes/footer.jsp" />
    </body>
</html>
