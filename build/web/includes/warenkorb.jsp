<%-- 
    Document   : warenkorb
    Created on : 02.03.2021, 16:52:02
    Author     : BirkeHeeren
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<jsp:useBean id="helper" class="art.dornrose.helper.ControllerHelper"/>

<table class="rechnung">
    <tr>
        <td><h2>Warenkorb</h2></td><td></td></tr>

    <jsp:include page="warenkorbinner.jsp" />

    <tr>
        <td><form action="PizzaBack" method="POST">
                <button name="back" class="kundespeichern"> zurücksetzen</button></form></td>

        <td>
            <c:choose>
                <c:when test="${bestellung.getTotalNumberOfPizzasChosen() gt 0 && sessionScope[helper.getLOGGEDIN()] eq true}">
                    <form action="BestellungInsert" method="post"> 
                        <button name="kostenpflichtig" class="weiter">kostenpflichtig bestellen</button>
                    </form>
                </c:when>
                <c:otherwise>

                </c:otherwise>
            </c:choose>                        
        </td></tr>
</table>