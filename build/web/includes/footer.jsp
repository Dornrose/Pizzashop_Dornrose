<%-- 
    Document   : footer
    Created on : 19.02.2021, 10:11:41
    Author     : BirkeHeeren
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<div class="footer">
    
    <p style="float: left;">Impressum:
        Pizza Dienst Rosso<br>
        Inhaberin Pepita Pizza<br>
        Pizzaweg 1a<br>
        22222 Pizzahausen
    <p>
        
    <h3 style="float: right;">
        <a href="bestellungen.jsp">Bestellungen abrufen</a>
    </h3>
</div>
