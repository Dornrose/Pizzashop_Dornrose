<%-- 
    Document   : headeranmeldung
    Created on : 26.02.2021, 14:14:44
    Author     : BirkeHeeren
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@page import="art.dornrose.helper.ControllerHelper"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<jsp:useBean id="helper" class="art.dornrose.helper.ControllerHelper"/>

<c:choose>

    <c:when test="${sessionScope[helper.getLOGGEDIN()] eq null || sessionScope[helper.getLOGGEDIN()] eq false}">
        <div id="anzeige" class="headerlogin">
            <h1><a href="<%=request.getContextPath()%>">Pizza&nbsp;Dienst&nbsp;Rosso</a></h1>
        </div> 
    </c:when>

    <c:otherwise>
        <div class="headerlogin">
            <form id="anzeige" action="Abmelden" method="post">
                <h1><a href="<%=request.getContextPath()%>">Pizza&nbsp;Dienst&nbsp;Rosso</a>
                    <c:out value="${sessionScope[helper.getBESTELLUNG()].getKunde().getVorname()}" /> 
                        <c:out value="${sessionScope[helper.getBESTELLUNG()].getKunde().getNachname()}" />
                    <button name="abmelden" class="abmeldenbutton" >abmelden</button></h1>
            </form>
        </div>
    </c:otherwise>

</c:choose>


