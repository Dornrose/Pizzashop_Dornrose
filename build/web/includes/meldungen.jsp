<%-- 
    Document   : meldungen
    Created on : 02.03.2021, 00:03:47
    Author     : BirkeHeeren
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@page import="art.dornrose.helper.ControllerHelper"%>
<%@page import="art.dornrose.enums.StatusLevel"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<jsp:useBean id="helper" class="art.dornrose.helper.ControllerHelper"/>

<div class="meldungouter">
<c:choose>
     <c:when test="${sessionScope[helper.getLOGGEDIN()] ne null && sessionScope[helper.getLOGGEDIN()] eq true}">
        <p class="meldung">Sie sind angemeldet.</p>
    </c:when>
    <c:otherwise>
        <p class="meldung">Sie sind nicht angemeldet.</p> 
    </c:otherwise>
</c:choose>
        
<p class="meldung">
<c:out value="${sessionScope[helper.getSTATUS()].getMeldung()}" />
</p>

</div>