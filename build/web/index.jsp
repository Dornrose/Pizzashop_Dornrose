<%-- 
    Document   : index
    Created on : 16.02.2021, 12:22:14
    Author     : BirkeHeeren
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Pizza Dienst Rosso</title>
        <link rel="stylesheet" href="css/main.css">
    </head>
    
    <body>
        <jsp:include page="includes/ensureConsistentState.jsp" />
        <jsp:include page="includes/header.jsp" />
        <jsp:include page="includes/warenkorb.jsp" />
        <jsp:include page="includes/headeranmeldung.jsp" />
        <img class="tomate" src="img/tomate.png" alt="Tomate"/>
        <jsp:include page="includes/meldungen.jsp" />
        <jsp:include page="includes/pizzas.jsp" />
        <jsp:include page="includes/footer.jsp" />
    </body>
</html>
